# MyFirstUnityGames_2019_2020

This a compilation of my first <b>Unity games</b> <br>
In this repository you'll find complete access to my source code.

## First Game : Don't Touch The Pillar (2019)

<figure>
  <img src="Pictures/dontTouchThePillar.png" width="500">
</figure>

Since it was my first one and I knew nothing whatsoever of Unity, I took inspiration from this
[tutorial](https://www.youtube.com/watch?v=JVbr7osMYTo) from "Mix and Jam" youtube channel. As the tutorial was rather guided <b>I was able to learn the basics of Unity</b> (<i>movements with the mouse, animations, sounds, gameplay loops, menus etc</i>).



## First Game Jam & VR experience (November 2019)

My second Game creation occurred during my first game Jam, the "Jamming Assembly" which gathered game, animation and sound jam. On a team of four we did a 3D prototype around the theme of "Space" about a cube which had to modify his capacity (<i>for eg : climb to wall, be on fire etc.</i>) to escape from his cell.
Even if, we couldn't release it on time (<i>malediction of the first jam, I supposed</i>),
<b> I learned a lot about C# Script in Unity, camera and a bit of level design </b>.

As part of one of my course, I also made a VR game with another student. The aim was to implement two algorithms described in two research papers. The first one proposed a way of doing [Handsfree omnidirectional VR navigation using head tilt](https://www.researchgate.net/publication/311575574_Handsfree_Omnidirectional_VR_Navigation_using_Head_Tilt). The second one, more difficult to implement, proposed a [Vr-Step algorithm](https://dl.acm.org/doi/10.1145/2858036.2858084) to transcribe real step movement into navigation inside the game. <br>
This project was an opportunity for me to work on <b>VR, to implement a VR click Tool and gain some knowledge on mobile build. </b>

<figure>
  <img src="Pictures/firstJam_1.png" width="250">
  <img src="Pictures/firstJam_2.png" width="250">
  <img src="Pictures/VRGame.png" width="250">
</figure>

<br>


---

## First Game published : Glitch'aat (January to March 2020)

I made this game with three of my friends during the GlobalGameJam. The theme was "repair" and so we tried as much as we can, to recreate a buggy and glitchy windows interface which you have to try to debug, with the help of a clumsy reparator. <br><br>
I worked around <b>100 hours on this project</b> in order to produce a polished game. <br>
During this process I learned a lot on unity's Canvas on which I have struggled a lot before, on animation, on interactivity and it showed me the work you have to put in, in order to finish a game, even a little one. <br><br>
We also did an [itch.io page](https://lumbirbwut.itch.io/glitchaat) where you can find our game.

<figure>
  <img src="Pictures/glitchaat_1.png" width="350">
  <img src="Pictures/glitchaat_2.png" width="350">
</figure>

<br>
